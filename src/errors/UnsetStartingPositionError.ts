export class UnsetStartingPositionError extends Error {
    constructor() {
        super('Shape starting position was no set');
    }
}