export class TooManyVertices extends Error {
    constructor() {
        super('This shape can not have more vertices');
    }
}