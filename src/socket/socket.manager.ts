import { config } from "../config.js";

export class SocketManager {
    socket: SocketIOClient.Socket;

    constructor() {
        this.socket = io(config.socketIO.host);
    }

    emit(event: string, data: any) {
        this.socket.emit(event, data);
    }

    listen(event: string, handler: (data: any) => any) {
        this.socket.on(event, handler);
    }
}