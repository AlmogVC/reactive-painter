import { Size } from "../canvas/canvas.manager.js";
import { Color } from "../models/color.model.js";
import { Position } from "../models/position.model.js";

export function getWindowSize(): Size {
  return {
    height: window.innerHeight,
    width: window.innerWidth,
  };
}

export function getPositionFromMouseEvent(mouseEvent: MouseEvent): Position {
  return {
    x: mouseEvent.offsetX,
    y: mouseEvent.offsetY,
  };
}

export function hexToRgb(hex: string): Color {
  const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function(m, r, g, b) {
    return r + r + g + g + b + b;
  });

  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

  if (!result) {
    throw new Error('failed to parse hex to rgb');
  }

  return {
    red: parseInt(result[1], 16),
    green: parseInt(result[2], 16),
    blue: parseInt(result[3], 16),
    alpha: 1,
  };
}
