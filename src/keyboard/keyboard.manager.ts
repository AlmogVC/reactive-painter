 export class KeyboardManager {
  private binding: Record<number, (...args: any) => any> = {};

  constructor() {
      this.initBinder();
  }

  initBinder() {
    document.addEventListener('keydown', (keyboardEvent: KeyboardEvent) => {
      const keyCode: number = keyboardEvent.keyCode;

      if (this.binding[keyCode]) this.binding[keyCode]();
    });
  }

  addBind(keyCode: number, action: (...args: any) => any) {
      this.binding[keyCode] = action;
  }
}
