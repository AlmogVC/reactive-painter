import { HistoryManager } from "./canvasHistory.manager.js";
import { Color, colorToString } from "../models/color.model.js";
import { Position, ZERO_POSITION } from "../models/position.model.js";

export interface Size {
  width: number;
  height: number;
}

export class CanvasManager {
  private canvas: HTMLCanvasElement;
  private context: CanvasRenderingContext2D;
  private drawingPosition: Position = ZERO_POSITION;
  private history: HistoryManager = new HistoryManager();
  private areDrawingFilled: boolean = false;

  constructor(
    canvasId: string,
    private size: Size = { height: 1000, width: 1000 }
  ) {
    this.canvas = document.getElementById(canvasId) as HTMLCanvasElement;

    if (!this.canvas) throw new Error(`id '${canvasId}' was not found in HTML`);

    const context = this.canvas.getContext("2d");

    if (!context) throw new Error(`Could not get canvas's context`);

    this.context = context;

    if (size) this.setSize(size);
    this.save();
  }

  setDrawingPosition(position: Position) {
    this.drawingPosition = position;
  }

  setSize(size: Size) {
    this.size = size;
    this.canvas.height = this.size.height;
    this.canvas.width = this.size.width;
  }

  setColor({ fill, line }: { fill?: Color; line?: Color }) {
    if (fill) this.context.fillStyle = colorToString(fill);
    if (line) this.context.strokeStyle = colorToString(line);
  }

  setDrawingFiller(isFilled: boolean) {
    this.areDrawingFilled = isFilled;
  }

  getDrawingFilledStatus() {
    return this.areDrawingFilled;
  }

  drawPaths(points: Position[], isClosed: boolean) {
    this.context.beginPath();

    const [startPosition, ...pointsWithoutStart] = points;

    this.context.moveTo(startPosition.x, startPosition.y);

    pointsWithoutStart.forEach((point) =>
      this.context.lineTo(point.x, point.y)
    );

    if (this.areDrawingFilled) this.context.fill();
    if (isClosed) this.context.closePath();
  
    this.context.stroke();
  }

  drawRectangle(size: Size = { height: 100, width: 100 }) {
    this.context.beginPath();
    this.context.rect(
      this.drawingPosition.x,
      this.drawingPosition.y,
      size.width,
      size.height
    );
    this.context.stroke();

    if (this.areDrawingFilled) {
      this.context.fillRect(
        this.drawingPosition.x,
        this.drawingPosition.y,
        size.width,
        size.height
      );
    }
  }

  drawCircle(radius: number = 1) {
    if (radius <= 0) radius = 1;
    this.context.beginPath();
    this.context.arc(
      this.drawingPosition.x,
      this.drawingPosition.y,
      radius,
      0,
      2 * Math.PI
    );

    this.context.stroke();
    if (this.areDrawingFilled) this.context.fill();
  }

  clear() {
    this.context.clearRect(0, 0, this.size.width, this.size.height);
  }

  addEventListener<K extends keyof HTMLElementEventMap>(
    type: K,
    listener: (this: HTMLCanvasElement, ev: HTMLElementEventMap[K]) => any,
    options?: boolean | AddEventListenerOptions
  ): void {
    this.canvas.addEventListener(type, listener);
  }

  drawImage(
    source: string,
    sourceCropPosition: Position = ZERO_POSITION,
    destinationPosition: Position = ZERO_POSITION,
    sourceSize: Size,
    destinationSize: Size
  ) {
    const image: HTMLImageElement = new Image();

    image.onload = () => {
      this.context.drawImage(
        image,
        sourceCropPosition.x,
        sourceCropPosition.y,
        sourceSize.width,
        sourceSize.height,
        destinationPosition.x,
        destinationPosition.y,
        destinationSize.width,
        destinationSize.height
      );
    };

    image.src = source;
  }

  getDataURL() {
    return this.canvas.toDataURL();
  }

  loadLast() {
    const lastCanvasState: string | undefined = this.history.popLast();

    if (!lastCanvasState) return;

    this.clear();
    this.drawImage(
      lastCanvasState,
      ZERO_POSITION,
      ZERO_POSITION,
      this.size,
      this.size
    );
  }

  save() {
    this.history.push(this.getDataURL());
  }
}
