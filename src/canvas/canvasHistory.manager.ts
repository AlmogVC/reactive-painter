export class HistoryManager {
    private history: string[];

    constructor() {
        this.history = [];
    }

    popLast() {
        return this.history.pop();
    }

    push(dataURL: string) {
        this.history.push(dataURL);
    }
}