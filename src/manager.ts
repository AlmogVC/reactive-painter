import { CanvasManager, Size } from "./canvas/canvas.manager.js";
import { KeyboardManager } from "./keyboard/keyboard.manager.js";
import { ShapeManager } from "./shape/shape.manager.js";
import {
  getWindowSize,
  getPositionFromMouseEvent,
  hexToRgb,
} from "./utils/utils.js";
import { KeyCodes } from "./keyboard/keycode.enum.js";
import { ComplexShape } from "./shape/complexShape.model.js";
import { Position } from "./models/position.model.js";
import { Color } from "./models/color.model.js";
import { ShapeType } from "./shape/shapeType.enum.js";
import { SocketManager } from "./socket/socket.manager.js";
import { MenuManager } from "./menu/menu.manager.js";
import { Menu } from "./menu/menu.js";
import { Shape } from "./shape/shape.model.js";

enum Mode {
  drawing = "DRAWING",
  creatingComplexShape = "CREATING_COMPLEX_SHAPE",
}

type ColorSliderProperties = {
  min: number;
  max: number;
  step: number;
  initialValue: number;
};

const OPACITY_SLIDER_VALUES: ColorSliderProperties = { min: 0, max: 1, step: 0.01, initialValue: 0.5 };

export class Manager {
  windowSize: Size = getWindowSize();
  canvasManager: CanvasManager = new CanvasManager("canvas", this.windowSize);
  keyboardManager: KeyboardManager = new KeyboardManager();
  shapeManger: ShapeManager = new ShapeManager();
  isMouseDown: boolean = false;
  currentMode: Mode = Mode.creatingComplexShape;
  socketManager: SocketManager;
  menuManager: MenuManager = new MenuManager();

  constructor() {
    this.socketManager = new SocketManager();

    this.initWindowEventListeners();
    this.initKeyboardEvents();
    this.initMouseEvents();
    this.initGeneralOptionsMenu();
    this.initShapeMenuButtons();

    this.socketManager.listen("shape", (shape) => {
      const createdShape = this.shapeManger.createShape(shape.type, shape);
      this.shapeManger.drawShape(createdShape, this.canvasManager);
    });

    this.socketManager.listen("REMOVE_ALL", () => {
      this.shapeManger.removeAll();
      this.canvasManager.clear();
      this.shapeManger.drawShapes(this.canvasManager);
    });
  }

  initWindowEventListeners() {
    window.addEventListener("resize", () => {
      this.windowSize = getWindowSize();
      this.canvasManager.setSize(this.windowSize);
    });
  }

  switchMode() {
    if (this.currentMode === Mode.creatingComplexShape) {
      this.currentMode = Mode.drawing;
    } else if (this.currentMode === Mode.drawing) {
      this.currentMode = Mode.creatingComplexShape;
    }
  }

  initShapeMenuButtons() {
    const shapeMenu: Menu = new Menu("shape-menu");

    (Object.keys(ShapeType) as ShapeType[]).forEach((shapeType: ShapeType) => {
      shapeMenu.addButton(shapeType, () => {
        this.shapeManger.changeShapeByType(shapeType);
        this.initShapeActionMenu();
      });
    });

    this.menuManager.addMenu(shapeMenu);
  }

  initShapeActionMenu() {
    const shapeActionMenu: Menu = new Menu("shape-action-menu");

    shapeActionMenu.addSlider(
      0.01,
      100,
      0.01,
      this.shapeManger.currentShape.getScale(),
      (event: any) => {
        const scale = Number(event.target.value);
        this.shapeManger.currentShape.setScale(scale);
      }
    );

    const lineColorMenu: Menu = this.initColorMenu("Line Color", "lineColor");
    const fillColorMenu: Menu = this.initColorMenu("Fill Color", "fillColor");

    shapeActionMenu.addSubMenu(fillColorMenu);
    shapeActionMenu.addSubMenu(lineColorMenu);

    this.menuManager.addMenu(shapeActionMenu);

    // this.menuManager.addMenu(this.initLineColorMenu(this.initFillColorMenu(shapeActionMenu)));
  }

  initColorMenu(
    menuName: string,
    colorPropertyName: keyof Pick<Shape, "lineColor" | "fillColor">
  ) {
    const menu: Menu = new Menu(undefined, menuName);

    menu.addColorPicker((hexColor: string) => {
      const color: Color = hexToRgb(hexColor);

      this.shapeManger.currentShape[colorPropertyName].red = color.red;
      this.shapeManger.currentShape[colorPropertyName].green = color.green;
      this.shapeManger.currentShape[colorPropertyName].blue = color.blue;
    });

    const { min, max, step, initialValue } = OPACITY_SLIDER_VALUES;
    menu.addSlider(min, max, step, initialValue, (event: any) => {
      const colorNumber = Number(event.target.value);
      this.shapeManger.currentShape[colorPropertyName].alpha = colorNumber;
    });
  
    return menu;
  }

  initGeneralOptionsMenu() {
    const generalOptionsMenu: Menu = new Menu("general-options-menu");

    generalOptionsMenu.addButton("Mode", () => this.switchMode());
    generalOptionsMenu.addButton("Clear", () => this.canvasManager.clear());
    generalOptionsMenu.addButton("Draw All", () =>
      this.shapeManger.drawShapes(this.canvasManager)
    );
    generalOptionsMenu.addButton("Remove Last", () => {
      this.shapeManger.removeLastShape();
      this.canvasManager.clear();
      this.shapeManger.drawShapes(this.canvasManager);
    });
    generalOptionsMenu.addButton("Remove All", () => {
      this.socketManager.emit("REMOVE_ALL", null);
      this.shapeManger.removeAll();
      this.canvasManager.clear();
      this.shapeManger.drawShapes(this.canvasManager);
    });

    this.menuManager.addMenu(generalOptionsMenu);
  }

  initKeyboardEvents() {
    this.keyboardManager.addBind(
      KeyCodes.KEY_R,
      this.canvasManager.clear.bind(this.canvasManager)
    );
    this.keyboardManager.addBind(
      KeyCodes.KEY_D,
      this.shapeManger.drawShapes.bind(this.shapeManger, this.canvasManager)
    );
    this.keyboardManager.addBind(
      KeyCodes.KEY_Q,
      this.shapeManger.changeShapeByType.bind(
        this.shapeManger,
        ShapeType.CIRCLE
      )
    );
    this.keyboardManager.addBind(
      KeyCodes.KEY_C,
      this.shapeManger.changeShapeByType.bind(
        this.shapeManger,
        ShapeType.COMPLEX
      )
    );
    this.keyboardManager.addBind(
      KeyCodes.KEY_T,
      this.shapeManger.changeShapeByType.bind(
        this.shapeManger,
        ShapeType.RECTANGLE
      )
    );
    this.keyboardManager.addBind(
      KeyCodes.KEY_I,
      this.shapeManger.changeShapeByType.bind(
        this.shapeManger,
        ShapeType.TRIANGLE
      )
    );
    this.keyboardManager.addBind(
      KeyCodes.KEY_E,
      this.shapeManger.changeShapeByType.bind(
        this.shapeManger,
        ShapeType.EQUILATERAL_TRIANGLE
      )
    );
    this.keyboardManager.addBind(KeyCodes.KEY_M, this.switchMode.bind(this));
    this.keyboardManager.addBind(
      KeyCodes.KEY_S,
      this.canvasManager.save.bind(this.canvasManager)
    );
    this.keyboardManager.addBind(
      KeyCodes.KEY_L,
      this.canvasManager.loadLast.bind(this.canvasManager)
    );
    this.keyboardManager.addBind(KeyCodes.PLUS, () =>
      this.shapeManger.currentShape.setScale(
        this.shapeManger.currentShape.getScale() + 0.1
      )
    );
    this.keyboardManager.addBind(KeyCodes.MINUS, () =>
      this.shapeManger.currentShape.setScale(
        this.shapeManger.currentShape.getScale() - 0.1
      )
    );
    this.keyboardManager.addBind(
      KeyCodes.KEY_F,
      () =>
        (this.shapeManger.currentShape.isFilled = !this.shapeManger.currentShape
          .isFilled)
    );
  }

  initMouseEvents() {
    this.canvasManager.addEventListener("click", this.onClick.bind(this));
    this.canvasManager.addEventListener(
      "mousedown",
      this.onMouseDown.bind(this)
    );
    this.canvasManager.addEventListener("mouseup", this.onMouseUp.bind(this));
    this.canvasManager.addEventListener(
      "mousemove",
      this.onMouseMove.bind(this)
    );
  }

  onClick(mouseEvent: MouseEvent) {
    const position: Position = getPositionFromMouseEvent(mouseEvent);

    if (this.currentMode === Mode.creatingComplexShape) {
      if (this.shapeManger.getCurrentShapeType() !== ShapeType.COMPLEX) return;

      (this.shapeManger.currentShape as ComplexShape).addVertex(position);
      this.canvasManager.clear();
      (this.shapeManger.currentShape as ComplexShape).drawPath(
        this.canvasManager
      );
    } else if (this.currentMode === Mode.drawing) {
      this.shapeManger.currentShape.setPosition(position);
      // this.shapeManger.currentShape.setColor({
      //   fill: getRandomColor(),
      //   line: getRandomColor(),
      // });
      // const createdShape = this.shapeManger.createCurrentShape();
      // this.socketManager.emit("created", createdShape);
      // this.shapeManger.drawCurrentShape(this.canvasManager);
    }
  }

  onMouseUp(mouseEvent: MouseEvent) {
    this.isMouseDown = false;
  }

  onMouseDown(mouseEvent: MouseEvent) {
    if (this.currentMode === Mode.creatingComplexShape) return;
    this.isMouseDown = true;
    this.shapeManger.currentShape.setPosition(
      getPositionFromMouseEvent(mouseEvent)
    );

    const createdShape = this.shapeManger.createCurrentShape();
    this.socketManager.emit("created", createdShape);
    this.shapeManger.drawCurrentShape(this.canvasManager);
  }

  onMouseMove(mouseEvent: MouseEvent) {
    if (!this.isMouseDown) return;

    if (this.currentMode === Mode.creatingComplexShape) {
      return;
    }
    this.shapeManger.currentShape.setPosition(
      getPositionFromMouseEvent(mouseEvent)
    );
    const createdShape = this.shapeManger.createCurrentShape();
    this.socketManager.emit("created", createdShape);
    this.shapeManger.drawCurrentShape(this.canvasManager);
  }
}
