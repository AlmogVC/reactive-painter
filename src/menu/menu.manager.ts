import { Menu } from "./menu.js";

export class MenuManager {
  menuContainerElement: HTMLDivElement;
  menus: Menu[] = [];

  constructor() {
    this.menuContainerElement = document.getElementById('menu-container') as HTMLDivElement;
  }
  
  addMenu(newMenu: Menu) {
    const existingMenu: Menu | undefined = this.menus.find(menu => menu.element.id === newMenu.element.id);

    if (existingMenu) this.menus = this.menus.filter(menu => menu.element.id !== newMenu.element.id);

    this.menus.push(newMenu);
    this.menuContainerElement.appendChild(newMenu.element);
  }
}
