export class Menu {
  element: HTMLDivElement;

  constructor(id?: string, name?: string) {
    this.element = document.createElement('div');
    
    if (name) {
      const menuNameElement: HTMLHeadingElement = document.createElement('h4');
      menuNameElement.textContent = name;
      this.element.appendChild(menuNameElement);
    }

    if (id) {
      const existingElement: HTMLElement | null = document.getElementById(id);
      if (existingElement && existingElement.parentElement) existingElement.parentElement.removeChild(existingElement);

      this.element.id = id;
      this.element.className = 'menu';
    }
  }

  addButton(text: string, action: () => any, className: string = 'button') {
    const shapeButton: HTMLButtonElement = document.createElement("button");
    shapeButton.textContent = text;

    if (className) shapeButton.className = className;

    shapeButton.addEventListener("click", action);
    this.element.appendChild(shapeButton);
  }

  addSlider(min: number, max: number, step: number, initialValue: number = min, action: (event: Event) => any) {
    const slider: HTMLInputElement = document.createElement('input');
    slider.type = 'range';
    slider.min = min.toString();
    slider.max = max.toString();
    slider.value = initialValue.toString();
    slider.step = step.toString();

    slider.addEventListener('input', action);

    this.element.appendChild(slider);
  }

  addSubMenu(menu: Menu) {
    this.element.appendChild(menu.element);
  }

  addColorPicker(action: (color: string) => any) {
    const colorPicker: HTMLInputElement = document.createElement('input');
    colorPicker.type = 'color'
    colorPicker.addEventListener('change', (event: any) => action(event.target.value));

    this.element.appendChild(colorPicker);
  }
}
