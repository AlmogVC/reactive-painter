import { Position, ZERO_POSITION } from "../models/position.model.js";
import { ShapeType } from "./shapeType.enum.js";
import { Triangle } from "./triangle.shape.js";

export class EquilateralTriangle extends Triangle {
  constructor(shapeType?: ShapeType, properties?: Record<string, any>) {
    super(shapeType || ShapeType.EQUILATERAL_TRIANGLE, properties);
    
    if (!properties) this.initEquilateralTriangle();
  }

  private initEquilateralTriangle() {
    this.vertices = [];
    this.startPosition = ZERO_POSITION;

    const point1: Position = this.startPosition;
    const point2: Position = { x: point1.x + this.scale / 2, y: point1.y + this.scale };
    const point3: Position = { x: point1.x - this.scale / 2, y: point1.y + this.scale };

    [point1, point2, point3].forEach(point => this.addVertex(point));
  }
}
