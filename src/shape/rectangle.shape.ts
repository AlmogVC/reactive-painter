import { Shape } from "./shape.model.js";
import { CanvasManager, Size } from "../canvas/canvas.manager.js";
import { ShapeType } from "./shapeType.enum.js";

export class Rectangle extends Shape {
  size: Size = { width: 10, height: 10 };

  constructor(shapeType?: ShapeType, properties?: Record<string, any>) {
    super(shapeType || ShapeType.RECTANGLE, properties);

    if (!properties) return;
    this.size = properties.size || this.size;
  }

  setSize(size: Size) {
    this.size = size;
  }

  draw(canvasManager: CanvasManager) {
    canvasManager.setDrawingPosition(this.startPosition);
    canvasManager.setDrawingFiller(this.isFilled);
    canvasManager.setColor({ fill: this.fillColor, line: this.lineColor });

    const drawSize: Size = {
      width: this.size.width * this.scale,
      height: this.size.height * this.scale,
    };

    canvasManager.drawRectangle(drawSize);
  }

  clone(): Rectangle {
    const clone: Rectangle = super.clone() as Rectangle;

    clone.size = this.size;

    return clone;
  }
}
