import { CanvasManager } from "../canvas/canvas.manager.js";
import { UnsetStartingPositionError } from "../errors/UnsetStartingPositionError.js";
import { ShapeType } from "./shapeType.enum.js";
import { Position, ZERO_POSITION } from "../models/position.model.js";
import { Shape } from "./shape.model.js";

export class ComplexShape extends Shape {
  protected vertices: Position[] = [];

  constructor(shapeType?: ShapeType, properties?: Record<string, any>) {
    super(shapeType || ShapeType.COMPLEX, properties);

    if (!properties) return;

    this.vertices = properties.vertices || this.vertices;
  }

  protected getVerticesRelativeToStartPosition(): Position[] {
    if (!this.startPosition) throw new UnsetStartingPositionError();

    const scaledVertices = this.vertices.map((vertex) => ({
      x: vertex.x * this.scale,
      y: vertex.y * this.scale,
    }));

    return scaledVertices.map((vertex) => ({
      x: vertex.x + this.startPosition.x,
      y: vertex.y + this.startPosition.y,
    }));
  }

  draw(canvasManager: CanvasManager) {
    canvasManager.setDrawingFiller(this.isFilled);

    canvasManager.setColor({ fill: this.fillColor, line: this.lineColor });
    canvasManager.drawPaths(this.getVerticesRelativeToStartPosition(), true);
  }

  drawPath(canvasManager: CanvasManager) {
    canvasManager.setDrawingFiller(false);

    canvasManager.setColor({ fill: this.fillColor, line: this.lineColor });
    canvasManager.drawPaths(this.getVerticesRelativeToStartPosition(), false);
  }

  addVertex(vertex: Position) {
    if (this.vertices.length === 0) {
      this.setPosition(vertex);
      vertex = ZERO_POSITION;
    } else {
      vertex = {
        x: vertex.x - this.startPosition.x,
        y: vertex.y - this.startPosition.y,
      };
    }

    this.vertices.push(vertex);
  }

  clone(): ComplexShape {
    const clone: ComplexShape = super.clone() as ComplexShape;
    
    clone.vertices = [...this.vertices];

    return clone;
  }
}
