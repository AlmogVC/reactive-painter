import { CanvasManager } from "../canvas/canvas.manager.js";
import { BLACK_COLOR, Color } from "../models/color.model.js";
import { Position, ZERO_POSITION } from "../models/position.model.js";
import { ShapeType } from "./shapeType.enum.js";

export abstract class Shape {
  public fillColor: Color = { ...BLACK_COLOR };
  public lineColor: Color = { ...BLACK_COLOR };
  protected startPosition: Position = { ...ZERO_POSITION };
  protected scale: number = 1;
  public isFilled: boolean = true;

  constructor(private type: ShapeType, properties?: Record<string, any>) {
    if (!properties) return;

    this.fillColor = properties.fillColor || this.fillColor;
    this.lineColor = properties.lineColor || this.lineColor;
    this.startPosition = properties.startPosition || this.startPosition;
    this.scale = properties.scale || this.scale;
    this.isFilled = properties.isFilled || this.isFilled;
  }

  setColor({ fill, line }: { fill?: Color; line?: Color }) {
    if (fill) this.fillColor = fill;
    if (line) this.lineColor = line;
  }

  getColor() {
    return { fill: this.fillColor, line: this.lineColor };
  }

  setPosition(position: Position) {
    this.startPosition = { ...position };
  }

  setScale(scale: number) {
    this.scale = scale;
  }

  getScale() {
    return this.scale;
  }

  abstract draw(canvasManager: CanvasManager, ...args: any): any;

  clone(): Shape {
    const clone: Shape = new (Object.getPrototypeOf(this).constructor)();

    clone.fillColor = this.fillColor;
    clone.lineColor = this.lineColor;
    clone.startPosition = this.startPosition;
    clone.scale = this.scale;
    clone.isFilled = this.isFilled;

    return clone;
  }
}
