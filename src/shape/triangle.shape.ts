import { TooManyVertices } from "../errors/TooManyVerticesError.js";
import { ComplexShape } from "./complexShape.model.js";
import { Position } from "../models/position.model.js";
import { ShapeType } from "./shapeType.enum.js";

const TRIANGLE_VERTICES_AMOUNT: number = 3;

export class Triangle extends ComplexShape {
  constructor(shapeType?: ShapeType, properties?: Record<string, any>) {
    super(shapeType || ShapeType.TRIANGLE, properties);
  }

  addVertex(vertex: Position) {
    if (this.vertices.length >= TRIANGLE_VERTICES_AMOUNT) {
      throw new TooManyVertices()
    }

    super.addVertex(vertex);
  }
}
