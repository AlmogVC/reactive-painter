import { Shape } from "./shape.model.js";
import { CanvasManager } from "../canvas/canvas.manager.js";
import { ShapeType } from "./shapeType.enum.js";

export class Circle extends Shape {
  protected radius: number = 10;

  constructor(shapeType?: ShapeType, properties?: Record<string, any>) {
    super(shapeType || ShapeType.CIRCLE, properties);
  }

  setRadius(radius: number) {
    this.radius = radius;
  }

  draw(canvasManager: CanvasManager) {
    canvasManager.setDrawingPosition(this.startPosition);
    canvasManager.setDrawingFiller(this.isFilled);
    canvasManager.setColor({ fill: this.fillColor, line: this.lineColor });
    canvasManager.drawCircle(this.radius * this.scale);
  }

  clone(): Circle {
    const clone: Circle = super.clone() as Circle;

    clone.radius = this.radius;

    return clone;
  }
}
