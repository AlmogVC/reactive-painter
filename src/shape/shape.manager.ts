import { CanvasManager } from "../canvas/canvas.manager.js";
import { ComplexShape } from "./complexShape.model.js";
import { Position } from "../models/position.model.js";
import { Shape } from "./shape.model.js";
import { ShapeType } from "./shapeType.enum.js";
import { Circle } from "./circle.shape.js";
import { EquilateralTriangle } from "./equilateralTriangle.shape.js";
import { Rectangle } from "./rectangle.shape.js";
import { Triangle } from "./triangle.shape.js";

const shapeCreationMap: Record<ShapeType, any> = {
  [ShapeType.CIRCLE]: Circle,
  [ShapeType.COMPLEX]: ComplexShape,
  [ShapeType.RECTANGLE]: Rectangle,
  [ShapeType.TRIANGLE]: Triangle,
  [ShapeType.EQUILATERAL_TRIANGLE]: EquilateralTriangle,
};

export class ShapeManager {
  private shapes: Shape[] = [];
  private shapeMap: Record<ShapeType, Shape> = {
    [ShapeType.CIRCLE]: new Circle(),
    [ShapeType.COMPLEX]: new ComplexShape(),
    [ShapeType.RECTANGLE]: new Rectangle(),
    [ShapeType.TRIANGLE]: new Triangle(),
    [ShapeType.EQUILATERAL_TRIANGLE]: new EquilateralTriangle(),
  };
  private currentShapeType: ShapeType = ShapeType.COMPLEX;
  public currentShape: Shape = this.shapeMap[this.currentShapeType];

  constructor() {
    this.changeShapeByType(ShapeType.COMPLEX);
  }

  createCurrentShape(position?: Position) {
    const newShape: Shape = this.currentShape.clone();

    if (position) newShape.setPosition(position);

    this.shapes.push(newShape);

    return newShape;
  }

  changeShapeByType(shapeType: ShapeType) {
    this.currentShapeType = shapeType;
    this.currentShape = this.shapeMap[this.currentShapeType];
  }

  changeShape(shape: Shape) {
    this.currentShape = shape;
  }

  getCurrentShape(): Shape {
    return this.currentShape;
  }

  getCurrentShapeType(): ShapeType {
    return this.currentShapeType;
  }

  drawShapes(canvasManager: CanvasManager) {
    this.shapes.forEach((shape) => shape.draw(canvasManager));
  }

  drawCurrentShape(canvasManager: CanvasManager) {
    this.currentShape.draw(canvasManager);
  }

  createShape(shapeType: ShapeType, shapeProperties: Record<string, any>) {
    const createdShape = new shapeCreationMap[shapeType](undefined, shapeProperties);
    this.shapes.push(createdShape);

    return createdShape;
  }

  removeAll() {
    this.shapes = [];
  }

  removeLastShape() {
    return this.shapes.pop();
  }
 
  drawShape(shape: Shape, canvasManager: CanvasManager) {
    shape.draw(canvasManager);
  }
}
