export interface Color {
  red: number;
  green: number;
  blue: number;
  alpha: number;
}

export const BLACK_COLOR: Color = {
  red: 0,
  green: 0,
  blue: 0,
  alpha: 1,
};

export const WHITE_COLOR: Color = {
  red: 0,
  green: 0,
  blue: 0,
  alpha: 1,
};

export function colorToString(color: Color) {
  const { red, green, blue, alpha } = color;

  return `rgba(${red}, ${green}, ${blue}, ${alpha})`;
}

export function getRandomColor(): Color {
  const randomColorNumber = () => Math.ceil(Math.random() * 255);

  const red: number = randomColorNumber();
  const green: number = randomColorNumber();
  const blue: number = randomColorNumber();
  const alpha: number = Math.random();

  return { red, green, blue, alpha };
}
